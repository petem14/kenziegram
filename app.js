const express = require("express");
const app = express();
const bodyParser = require('body-parser')
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }))
// app.set('view engine', 'pug')

var multer = require('multer');
var upload = multer({ dest: 'public/uploads/' });
var type = upload.single('myFile');

var fs = require('fs');

let comments = {}
fs.readFile('./comments.json', (err, data)=> {
    comments = JSON.parse(data)
})

app.post('/upload/', type, (req, res) => {
    res.redirect(`/${req.file.filename}`)
})


app.post('/:filename/', type, (req, res) => {
    if (req.body != {}) {
        if (comments[req.params.filename] === undefined) {
            comments[req.params.filename] = []

            let newComment = {}
            newComment.name = req.body.name
            newComment.comment = req.body.comment

            comments[req.params.filename].push(newComment)


        } else {
            let newComment = {}
            newComment.name = req.body.name
            newComment.comment = req.body.comment
            comments[req.params.filename].push(newComment)
        }
    }

    fs.writeFileSync('comments.json', JSON.stringify(comments))

    console.log(comments[req.params.filename])
    let postHTML = `<h1>SUCCESS</h1>
                <a href="/">Go back</a><br>
                <h1>${req.params.filename}</h1>
                <img src="/uploads/${req.params.filename}">
                <h2>Comments:${comments[req.params.filename] ? comments[req.params.filename].length : 0}</h2>
                <form action='http://localhost:3000/${req.params.filename}' 
                        method='POST' 
                        enctype='application/x-www-form-urlencoded'>
                    <label>Name</label>
                    <input type="text" name='name'><br>
                    <label>Comment</label>
                    <textarea name='comment'></textarea>
                    <button type='submit'>Submit</button>
                </form>`
    let commentDivs = '<h1>Recent Comments:</h1>'
    if(comments[req.params.filename]){
        for (comment of comments[req.params.filename]) {
            if (comment.name !== undefined && comment.comment !== undefined)
                commentDivs += `<div>${comment.name} commented: ${comment.comment}</div>`
        }
    }
    postHTML += commentDivs
    res.send(postHTML)
})

app.get('/:filename/', (req, res) => { 
    let postHTML = '<h1>SUCCESS</h1>'
    postHTML += `<a href="/">Go back</a><br>
                <h1>${req.params.filename}</h1>
                <img src="/uploads/${req.params.filename}">
                <h2>Comments:${comments[req.params.filename] ? comments[req.params.filename].length : 0}</h2>
                <form action='http://localhost:3000/${req.params.filename}' 
                        method='POST' 
                        enctype='application/x-www-form-urlencoded'>
                    <label>Name<label>
                    <input type="text" name='name'><br>
                    <label>Comment</label><br>
                    <textarea name='comment'></textarea>
                    <button type='submit'>Submit</button>
                </form>`
    let commentDivs = '<h1>Recent Comments:</h1>'
    if(comments[req.params.filename]){
        for (comment of comments[req.params.filename]) {
            if (comment.name !== undefined && comment.comment !== undefined)
                commentDivs += `<div>${comment.name} commented: ${comment.comment}</div>`
        }
    }
    postHTML += commentDivs
    res.send(postHTML)
})

app.get('/', (req, res) => {
    const path = './public/uploads';
    fs.readdir(path, function (err, items) {
        html = ''
        for (imageName of items) {
            html += `<a href='/${imageName}'><img src="/uploads/${imageName}"></a>`
        }
        html += `<form action="http://localhost:3000/upload" method="POST" enctype="multipart/form-data">
                    <div>
                        <div>
                            <span>File</span>
                            <input name="myFile" type="file">
                        </div>
                    </div>
                    <button type="submit" class="btn">Submit</button>
                </form>`
        res.send(html);
    });
})

const port = 3000;

app.listen(port, () => console.log(`Server started on port ${port}`));